/* Dolly
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef QOCOON_H
#define QOCOON_H

#include <QMap>
#include <QMainWindow>
#include <QUndoView>

#include "skeletonwidget.h"
#include "bonewidget.h"

#include "drydockwidget.h"
#include "dry.h"

class View3D;

class Qocoon: public QMainWindow, public Object
{
    Q_OBJECT
    DRY_OBJECT(Qocoon, Object)

public:
    explicit Qocoon(Context* context);
    ~Qocoon();

    bool load(String filename);

    QDockWidget* createDockWidget(QWidget* w, Qt::DockWidgetArea area)
    {
        QDockWidget* dockWidget{ new QDockWidget(w->objectName(), this) };
        dockWidget->setWidget(w);
        dockWidget->setObjectName(w->objectName() + "Dock");
        w->setParent(dockWidget);

        addDockWidget(area, dockWidget);
        return dockWidget;
    }

    static String projectLocation();
    static bool inResourceFolder(const String& fileName);
    static String trimmedResourceName(const String& fileName);
    static String containingFolder(const String& path);
    static String absoluteResourceFolder(const String& path);
    static String locateResourceRoot(String resourcePath);

signals:
    void activeModelChanged(AnimatedModel* model);

    void resourceFoldersChanged();
    void recentProjectsChanged();
    void currentProjectChanged();

public slots:
    void openRecent() { open(sender()->objectName()); }

protected:
    void resizeEvent(QResizeEvent *event) override;

private slots:
    void open(QString filename = "");
    bool savePrefab();
    void addResourceFolder();
    void updateRecentProjectMenu();
    void updateAvailableActions();

    void toggleFullView(bool toggled);
    void about();
    void openUrl();
    void test(bool checked);
    void undo();
    void redo();

    void closeProject();

private:
    QMenu* createMenuBar();
    void createToolBar();
    void loadSettings();
    void updateRecent();

    std::vector<QAction*> projectDependentActions_;
    QToolBar* mainToolBar_;
    QUndoView* undoView_;
    View3D* view3d_;
    SkeletonWidget* skeletonWidget_;
    BoneWidget* boneWidget_;
    AnimatedModel* model_;
    Node* cellNode_;

    QMenu* recentMenu_;
    std::vector<QDockWidget*> hiddenDocks_;
    QAction* fullViewAction_;
    void createScene();
};

#endif // QOCOON_H
