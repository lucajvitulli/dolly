#ifndef GARNISH_H
#define GARNISH_H

#include <QPixmap>

#include <QObject>

class View3D;

class Garnish: public QObject
{
    Q_OBJECT
public:
    explicit Garnish(View3D* parent = nullptr);

    operator QPixmap() const {
        return pixmap_;
    }

private:
    QPixmap pixmap_;

};

#endif // GARNISH_H
