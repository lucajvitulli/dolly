/* Dolly
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QResizeEvent>
#include <QPainter>
#include "weaver.h"
#include "view3d.h"

View3D::View3D(Context* context, QWidget* parent): DryWidget(context, parent),
    scene_{ nullptr },
    jib_{ nullptr },
    renderTexture_{ nullptr },
    previousMousePos_{ width() / 2, height() / 2 },
    image_{ nullptr },
    pixmap_{ width(), height() },
    garnish_{ this },
    continuousUpdate_{ false }
{
    setObjectName("view3d");
    setMinimumSize(0200, 0140);
    setMouseTracking(true);

    Node* jibNode{ new Node{ context_ } };
    jibNode->SetTemporary(true);
    jib_ = jibNode->CreateComponent<Jib>();
    jib_->SetTemporary(true);

    pixmap_.fill(Qt::transparent);
}

void View3D::setScene(Scene* scene)
{
    if (scene == scene_)
        return;

    scene_ = scene;
    Node* jibNode{ jib_->GetNode() };

    jibNode->SetScene(scene_);
    jibNode->SetPosition(Vector3::ONE * 2.3f);
    jibNode->LookAt(Vector3::UP);

    createRenderTexture();
}

void View3D::createRenderTexture()
{
    if (renderTexture_)
        renderTexture_->GetRenderSurface()->Release();

    renderTexture_ = new Texture2D(context_);
    renderTexture_->SetSize(width(), height(), Graphics::GetRGBAFormat(), TEXTURE_RENDERTARGET);
    renderTexture_->GetRenderSurface()->SetUpdateMode(SURFACE_MANUALUPDATE);

    updateViewport();
}

void View3D::updateViewport()
{
    RenderSurface* renderSurface{ renderTexture_->GetRenderSurface() };

    if (renderSurface->GetViewport(0))
    {
        renderSurface->GetViewport(0)->SetScene(scene_);
    }
    else
    {
        SharedPtr<Viewport> viewport{ new Viewport{ context_, scene_, jib_, GetSubsystem<Renderer>()->GetDefaultRenderPath() } };
        renderSurface->SetViewport(0, viewport);
    }

    if (height())
    {
        const float portraitRatio{ Min(1.0f, static_cast<float>(width()) / height()) };

        jib_->SetZoom(portraitRatio);
    }

    updateView();
}

void View3D::updateView(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    updateView();
}

void View3D::updateView()
{
    if (!renderTexture_)
        return;

    renderTexture_->GetRenderSurface()->QueueUpdate();
    GetSubsystem<Weaver>()->requestUpdate();

    SubscribeToEvent(E_ENDRENDERING, DRY_HANDLER(View3D, paintView));
}

void View3D::resizeEvent(QResizeEvent* /*event*/)
{
    if (!GetSubsystem<Weaver>())
        return;

    createRenderTexture();
}

void View3D::paintEvent(QPaintEvent* /*event*/)
{
    if (!isVisible())
        return;

    paintView();

    if (continuousUpdate_)
        updateView();
}

void View3D::paintView(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    repaint();
}

void View3D::paintView()
{
    updatePixmap();

    const int drawWidth{  CeilToInt(pixmap_.width() * static_cast<float>(height()) / pixmap_.height()) };
    const int dX{ width() - drawWidth };

    QPainter p{};
    QColor backgroundColor{ toQColor(GetSubsystem<Renderer>()->GetDefaultZone()->GetFogColor(), false) };

//    if (scene_)
//    {
//        if (Zone* zone{ scene_->GetComponent<Zone>() })
//            backgroundColor = toQColor(zone->GetFogColor());

//        if (backgroundColor.alpha() < 255)
//            Weaver::paintCheckerboard(this, 12);

//        p.begin(this);
//        p.fillRect(rect(), backgroundColor);
//        p.end();
//    }

    p.begin(this);
    p.drawPixmap(QRect{ dX / 2, 0, drawWidth, height() }, pixmap_);
    p.end();
}

void View3D::updatePixmap()
{
    Image* image{ renderTexture_->GetImage() };

    if (!image || renderTexture_->GetRenderSurface()->IsUpdateQueued())
        return;

    if (image_ != image)
    {
        image_  = renderTexture_->GetImage();
        pixmap_ = toPixmap(image_);

        UnsubscribeFromEvent(E_ENDRENDERING);
    }
}

void View3D::mouseMoveEvent(QMouseEvent* event)
{
    if (!scene_)
        return;

    Qt::MouseButtons buttons{ QApplication::mouseButtons() };

    if (buttons & Qt::MiddleButton)
    {
        const QPoint dPos{ QCursor::pos() - previousMousePos_ };
        const Vector2 dVec{ dPos.x() * 0.00125f, dPos.y() * 0.001666f };

        jib_->move(Vector3{ dVec }, MT_ROT);

        wrapCursor(event->pos());

        updateView();
    }
    else
    {
//        cursor->HandleMouseMove();
    }

    previousMousePos_ = QCursor::pos();
}

void View3D::wheelEvent(QWheelEvent* event)
{
    if (!scene_)
        return;

    jib_->move(Vector3::UP * event->delta() * -0.001f, MT_FOV);
    updateView();
}

void View3D::wrapCursor(const QPoint& pos)
{
    if (pos.x() <= 0)
        QCursor::setPos(QCursor::pos() + QPoint{ width() - 2, 0 });
    else if (pos.x() >= width() - 1)
        QCursor::setPos(QCursor::pos() - QPoint{ width() - 2, 0 });
    else if (pos.y() <= 0)
        QCursor::setPos(QCursor::pos() + QPoint{ 0, height() - 2 });
    else if (pos.y() >= height() - 1)
        QCursor::setPos(QCursor::pos() - QPoint{ 0, height() - 2 });
}
