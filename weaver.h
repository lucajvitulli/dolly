/* Dolly
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef WEAVER_H
#define WEAVER_H

#include "dry.h"
#include <QApplication>
#include <QMimeData>
#include <QPixmap>
#include <QPainter>
#include <QByteArray>

#define FILENAME_PROJECT Weaver::applicationName().append(".xml").toLatin1().data()

class Qocoon;
enum UserRoles{ FileName = Qt::UserRole, DryTypeHash, ID, ObjectPointer };

class Weaver : public QApplication,  public Application
{
    Q_OBJECT
    DRY_OBJECT(Weaver, Application)

public:
    Weaver(Context* context, int & argc, char** argv);
    virtual ~Weaver();

    static Context* context() { return context_s;}
    static Context* context_s;

    String locateResourceRoot(String path, bool acceptEngineFolder);
    String projectLocation() const;

    void Setup() override;
    void Start() override;
    void Stop() override;
    void exit();
    void runFrame();

    static void paintCheckerboard(QPaintDevice* paintDevice, int squareSize = 8)
    {
        if (squareSize == 0)
            return;

        QPixmap checkerboard{ 2 * squareSize, 2 * squareSize }; // Create pattern
        checkerboard.fill(QColor("#3a3b3c"));
        QPainter cP{ &checkerboard };
        cP.setBrush(QColor("#acabad"));
        cP.setPen(Qt::transparent);

        for (bool second: { false, true })
            cP.drawRect(second * squareSize, second * squareSize, squareSize, squareSize);

        cP.end();

        QPainter p{ paintDevice }; // Fill target with checkboard pattern
        p.setBrush(QBrush(checkerboard));
        p.setPen(QPen(QColor("#555555"), 2));
        p.drawRect(QRect(0, 0, paintDevice->width(), paintDevice->height()));
        p.end();
    }

public slots:
    void onTimeout();
    void requestUpdate() { requireUpdate_ = true; }

private:
    void handleArgument();
    bool looksLikeUrho(QString engineFolder);
    bool looksLikeUrho(String engineFolder);

    String argument_;
    bool requireUpdate_;
    Qocoon* qocoon_;
};

static QString toQString(String string)
{
    return QString{ string.CString() };
}

static String toString(QString qString)
{
    return String{ qString.toLatin1().data() };
}

static QColor toQColor(const Color color, bool alpha = true)
{
    return QColor::fromRgbF( std::min(std::max(0.0f, color.r_), 1.0f),
                             std::min(std::max(0.0f, color.g_), 1.0f),
                             std::min(std::max(0.0f, color.b_), 1.0f),
                    (alpha ? std::min(std::max(0.0f, color.a_), 1.0f) : 1.0f) );
}

static Color toColor(const QColor qColor)
{
    return Color(qColor.redF(), qColor.greenF(), qColor.blueF(), qColor.alphaF());
}

static QPixmap toPixmap(Image* image)
{
    return QPixmap::fromImage(QImage{
                    image->GetData(),
                    image->GetWidth(),
                    image->GetHeight(),
                    QImage::Format_RGBA8888 });
}

static QMap<int, QVariant> extractDataMap(const QMimeData* mime)
{
    QByteArray encoded{ mime->data("application/x-qabstractitemmodeldatalist") };
    QDataStream stream(&encoded, QIODevice::ReadOnly);
    QMap<int, QVariant> roleDataMap{};

    while (!stream.atEnd())
    {
        int row, col;
        stream >> row >> col >> roleDataMap;
    }

    for (const QVariant& val: roleDataMap.values())
    {
        const int key{ roleDataMap.key(val) };
        Log::Write(LOG_INFO, String(key) + ": " + toString(val.toString()));
    }

    return roleDataMap;
}

template <class T> static bool isOfType(const QMimeData* mimeData)
{
    QMap<int, QVariant> dataMap{ extractDataMap(mimeData) };

    if (dataMap.keys().contains(DryTypeHash))
        return T::GetTypeStatic().ToHash() == dataMap[DryTypeHash].toUInt();
    else
        return false;
}

static bool isOfBinaryType(const QMimeData* mimeData)
{
    return isOfType<Model>(mimeData)
        || isOfType<Animation>(mimeData)
        || isOfType<Texture2D>(mimeData)
        || isOfType<Sound>(mimeData);
}

class DryQObject: public QObject
{
    Q_OBJECT

public:
    DryQObject(Object* object, QObject* parent = nullptr): QObject(parent),
        object_{ object } {}

    Object* ptr() const { return object_; }

private:
    Object* object_;
};

static Object* toObject(const QVariant& variant)
{
    if (!variant.isValid())
        return nullptr;

    QObject* qObject{ qvariant_cast<QObject*>(variant) };

    if (qObject)
        return qobject_cast<DryQObject*>(qObject)->ptr();
    else
        return nullptr;
}

static std::vector<unsigned> extractNumbers(const QByteArray& seg)
{
    std::vector<unsigned> numbers{};

//    for (char c: seg)
//    {
//        // if mirror or last valid, push value
//    }
}


#endif // WEAVER_H
