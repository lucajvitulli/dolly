/* Dolly
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QSettings>
#include <QComboBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QResizeEvent>
#include <QApplication>
#include <QDesktopWidget>

#include "drydockwidget.h"

DryDockWidget::DryDockWidget(DryWidget* widget, QWidget* parent): QDockWidget(parent)
{
    applyWidget(widget);

    connect(this, SIGNAL(topLevelChanged(bool)), SLOT(onTopLevelChanged(bool)));
}

void DryDockWidget::applyWidget(DryWidget* newWidget)
{
    if (dryWidget_ != newWidget)
    {
        newWidget->setParent(this);
        dryWidget_ = newWidget;
        setWidget(newWidget);
        resizeEvent(nullptr);

        setObjectName(dryWidget_->objectName() + "Dock");
    }
}

void DryDockWidget::onTopLevelChanged(bool topLevel)
{
    if (!topLevel)
        return;

    const float ratio{ static_cast<float>(width()) / height() };
    const float maxRatio{ 2.3f + 1.7f * (ratio > 1.0f) };

    if (ratio > maxRatio)
    {
        resize(height() * maxRatio, height());
    }
    else if (ratio < 1.0f / maxRatio)
    {
        resize(width(), width() * maxRatio);
    }
}

void DryDockWidget::resizeEvent(QResizeEvent* /*event*/)
{
    const float bias{ 0.5f * static_cast<float>(height()) / QApplication::desktop()->height() };
    const float ratio{ static_cast<float>(width()) / height() - bias };

    if (features() & DockWidgetVerticalTitleBar)
    {
        if (ratio < 1.23f)
        {
            setFeatures(features() ^ DockWidgetVerticalTitleBar);

            if (dryWidget_)
                dryWidget_->setOrientation(Qt::Vertical);
        }
    }
    else
    {
        if (ratio > 1.42f)
        {
            setFeatures(features() | DockWidgetVerticalTitleBar);

            if (dryWidget_)
                dryWidget_->setOrientation(Qt::Horizontal);
        }
    }
}
