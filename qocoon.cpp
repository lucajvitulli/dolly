/* Dolly
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDebug>

#include <QMenuBar>
#include <QToolBar>
#include <QStatusBar>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QLabel>
#include <QSettings>
#include <QFileDialog>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QUrl>
#include <QAction>
#include <QResizeEvent>
#include <QVBoxLayout>

#include "view3d.h"

#include "weaver.h"
#include "qocoon.h"

Qocoon::Qocoon(Context* context): QMainWindow(), Object(context),
    projectDependentActions_{},
    mainToolBar_{ nullptr },
    hiddenDocks_{},
    undoView_{ new QUndoView{ this } },
    view3d_{ new View3D{ context, this } },
    skeletonWidget_{ new SkeletonWidget{ this } },
    boneWidget_{ new BoneWidget{ this } },
    model_{ nullptr },
    cellNode_{ nullptr },
    fullViewAction_{ new QAction{QIcon{ ":/FullView" }, "Fullscreen View", this} },
    recentMenu_{ nullptr }
{
    context_->RegisterSubsystem(this);
    setWindowIcon(QIcon{ ":/Icon" });
    setCentralWidget(view3d_);
    setTabPosition(Qt::LeftDockWidgetArea, QTabWidget::North);

    undoView_->setEmptyLabel("Initial");
    QDockWidget* undoDockWidget{ new QDockWidget{ "History", this } };
    undoDockWidget->setObjectName("History");
    undoDockWidget->setWidget(undoView_);
    addDockWidget(Qt::LeftDockWidgetArea, undoDockWidget);

    createDockWidget(skeletonWidget_, Qt::LeftDockWidgetArea);
    createDockWidget(boneWidget_, Qt::LeftDockWidgetArea);
    connect(this, SIGNAL(activeModelChanged(AnimatedModel*)), skeletonWidget_, SLOT(updateModel(AnimatedModel*)));
    connect(skeletonWidget_, SIGNAL(currentBoneChanged(Node*)), boneWidget_, SLOT(setBone(Node*)));

    setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);
    setCorner(Qt::BottomLeftCorner,  Qt::BottomDockWidgetArea);

    fullViewAction_->setCheckable(true);
    fullViewAction_->setShortcut(QKeySequence{ "F11" });
    addAction(fullViewAction_);
    connect(fullViewAction_, SIGNAL(triggered(bool)), this, SLOT(toggleFullView(bool)));

    connect(this, SIGNAL(currentProjectChanged()), SLOT(updateAvailableActions()));
    ///////!
    //    connect(undoView_->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
    //            view3d_, SLOT(updateView()));
    //    connect(undoView_->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
    //            view3d_, SLOT(updateView()));
    ///////^

    createToolBar();
    createMenuBar();
    setStatusBar(new QStatusBar{ this });
    loadSettings();
    updateRecent();

    createScene();
    load("Models/Dolly.mdl");
    statusBar()->showMessage("Welcome!");

    show();
}

Qocoon::~Qocoon()
{
    QSettings settings{};

    if (fullViewAction_->isChecked())
        fullViewAction_->trigger();

    settings.setValue("geometry", saveGeometry());
    settings.setValue("state", saveState());
}

void Qocoon::loadSettings()
{
    QSettings settings{};

    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("state").toByteArray());
}

void Qocoon::createToolBar()
{
    mainToolBar_ = new QToolBar("Toolbar");
    mainToolBar_->setObjectName("MainToolbar");

    QString playName{ "Test" };
    QAction* playAction{ new QAction(playName, this) };
    playAction->setIcon(QIcon(":/" + playName ));
    playAction->setCheckable(true);
//    action->setShortcut(QKeySequence("Ctrl+Shift+" + QString::number(mode)));
    mainToolBar_->addAction(playAction);
    connect(playAction, SIGNAL(triggered(bool)), this, SLOT(test(bool)));

    mainToolBar_->addSeparator();

    for (bool undo: { true, false })
    {
        QString  actionName{ QString(undo ? "Un" : "Re") + "do" };
        QAction* action{ new QAction(actionName, this) };
        action->setObjectName(actionName + "action");
        action->setIcon(QIcon(":/" + actionName ));
        action->setShortcut(QKeySequence("Ctrl+" + QString(undo ? "" : "Shift+") + "Z"));

        if (undo)
            connect(action, SIGNAL(triggered(bool)), this, SLOT(undo()));
        else
            connect(action, SIGNAL(triggered(bool)), this, SLOT(redo()));

        mainToolBar_->addAction(action);
    }

    addToolBar(mainToolBar_);
}

void Qocoon::createScene()
{
    Scene* scene{ new Scene{ context_ } };
    scene->CreateComponent<Octree>();
    view3d_->setScene(scene);

    Node* sunNode{ scene->CreateChild("Sun") };
    Light* sun{ sunNode->CreateComponent<Light>() };
    sunNode->SetPosition(10 * Vector3{ 0.55f, 2.3f, 1.7f });
    sunNode->LookAt(Vector3::ZERO);
    sun->SetCastShadows(true);
    sun->SetRange(100);

    Node* dollNode{ scene->CreateChild("Ragdoll") };
    model_ = dollNode->CreateComponent<AnimatedModel>();
    model_->SetCastShadows(true);

    cellNode_ = scene->CreateChild("Cell");
    StaticModel* cell{ cellNode_->CreateComponent<StaticModel>() };
    cell->SetModel(CACHE(Model)("Models/Cell.mdl"));
}

void Qocoon::test(bool checked)
{
    QAction* playAction{ static_cast<QAction*>(sender()) };

    if (checked)
        playAction->setIcon(QIcon(":/TestIn"));
    else
        playAction->setIcon(QIcon(":/Test"));
}

QMenu* Qocoon::createMenuBar()
{
    Weaver* weaver{ GetSubsystem<Weaver>() };

    QMenu* fileMenu{ new QMenu("File") };

    fileMenu->addAction(QIcon(":/Open"), "Open...", this, SLOT(open()), QKeySequence("Ctrl+O"));
    recentMenu_ = fileMenu->addMenu("Recent");
    connect(this, SIGNAL(recentProjectsChanged()), SLOT(updateRecentProjectMenu()));
    fileMenu->addSeparator();
    projectDependentActions_.push_back(fileMenu->addAction(QIcon(), "Close",
                                                           this, SLOT(closeProject())));
    projectDependentActions_.push_back(fileMenu->addAction(QIcon(":/Save"), "Save Project...",
                                                           this, SLOT(savePrefab()), QKeySequence("Ctrl+S")));
    fileMenu->addSeparator();
    projectDependentActions_.push_back(fileMenu->addAction(QIcon(":/Open"), "Add Resource Folder...",
                                                           this, SLOT(addResourceFolder())));
    fileMenu->addSeparator();
    fileMenu->addAction(QIcon(":/Quit"), "Exit", weaver, SLOT(quit()), QKeySequence("Ctrl+Q"));
    menuBar()->addMenu(fileMenu);

    QMenu* viewMenu{ new QMenu("View") };
    viewMenu->addAction(fullViewAction_);
    menuBar()->addMenu(viewMenu);

    QMenu* helpMenu{ new QMenu("Help") };
    helpMenu->addAction(QIcon(":/About"), tr("About %1...").arg(Weaver::applicationDisplayName()), this, SLOT(about()));
    menuBar()->addMenu(helpMenu);

    updateAvailableActions();

    return fileMenu;
}

void Qocoon::updateAvailableActions()
{
//    for (QAction* action: projectDependentActions_)
//        action->setEnabled(project() != nullptr);
}

void Qocoon::updateRecentProjectMenu()
{
    QSettings settings{};
    QStringList recentProjects{( settings.value("recentprojects").toStringList() )};

    recentMenu_->clear();

    for (const QString& recent: recentProjects)
    {
        QString displayName{ recent.left(recent.length() - (String(FILENAME_PROJECT).Length() + 1)) };
        recentMenu_->addAction(displayName, this, SLOT(openRecent()))->setObjectName(recent);
    }
}

void Qocoon::updateRecent()
{
    QSettings settings{};
    QStringList recentProjects{( settings.value("recentprojects").toStringList() )};
//    QString currentProject{ toQString(projectLocation() + FILENAME_PROJECT) };

//    for (const QString& recent: recentProjects)
//    {
//        if (!QFileInfo(recent).exists() || currentProject == recent )
//            recentProjects.removeOne(recent);
//    }

//    if (project_)
//        recentProjects.push_front(currentProject);

//    while (recentProjects.size() > 10)
//        recentProjects.pop_back();

    settings.setValue("recentprojects", recentProjects);

    emit recentProjectsChanged(); /// Should not be emitted with no change
}

void Qocoon::open(QString filename) /// Open model or prefab
{
//    if (filename.isEmpty()) // Pick a project through a file dialog
//    {
//        QString startPath{ project_ ? toQString(projectLocation())
//                                    : QStandardPaths::writableLocation(QStandardPaths::HomeLocation) };

//        String selectedProject{ toString(QFileDialog::getOpenFileName(nullptr, tr("Open Project"), startPath, "*.XML")) };
//        if (selectedProject.IsEmpty()) // Cancelled
//            return;

//        filename = toQString(selectedProject);
//    }

//    loadProject(filename);
}

bool Qocoon::load(String filename)
{
    model_->SetModel(CACHE(Model)(filename));
    model_->SetMaterial(CACHE(Material)("Materials/VCol.xml"));

    unsigned lastSlash{ filename.FindLast('/') };

    if (lastSlash == String::NPOS)
        lastSlash = 0;
    else
        ++lastSlash;

    const String nodeName{ filename.Substring(
                           lastSlash,
                           filename.Length() - lastSlash - 4) };

    model_->GetNode()->SetName(nodeName);

    float scale{ 1.0f };

    for (int s{0}; s < 3; ++s)
    {
        float size{ scale };
        switch (s) {
        default:
        case 0: size = model_->GetBoundingBox().Size().x_;
        break;
        case 1: size = model_->GetBoundingBox().Size().x_;
        break;
        case 2: size = model_->GetBoundingBox().Size().x_;
        break;
        }
        size *= 2.3f;

        if (size > scale)
            scale = size;
    }

    cellNode_->SetScale(scale);
    cellNode_->SetPosition(Vector3::UP * scale * 0.5f);

    statusBar()->showMessage(toQString("Opened: " + filename));
    emit activeModelChanged(model_);

    return true;

//    const String path{ AddTrailingSlash(GetPath(toString(filename))) };

//    if (project_ && project_->location() == path)
//    {
//        statusBar()->showMessage("Project already open");
//        return false;
//    }

//    if (QFileInfo(filename).exists())
//    {
//        SharedPtr<XMLFile> projectFile{ GetSubsystem<ResourceCache>()->GetTempResource<XMLFile>(toString(filename)) };

//        if (!projectFile.IsNull() && !projectFile->GetRoot("project").IsNull())
//        {
//            SharedPtr<Project> newProject{ new Project(context_) };

//            newProject->setLocation(path);

//            if (newProject->LoadXML(projectFile->GetRoot("project")))
//            {

//                project_ = newProject;
//                //            setScene(EMPTY_SCENE());
//                hololith_->setCurrentIndex(1);

//                setWindowTitle(toQString(project()->name()));
//                statusBar()->showMessage("Opened project " + toQString(project_->name()));
//                updateRecentProjects();

//                emit currentProjectChanged();
//                return true;
//            }
//        }
//    }

}

bool Qocoon::savePrefab()
{
//    if (project_)
//        return project_->Save();
//    else
        return false;
}

void Qocoon::closeProject() //\\\ Should check for modified files
{
//    if (!project_)
//        return;

//    hololith_->setCurrentIndex(0);

//    project_->remove();
//    project_ = nullptr;

//    emit currentProjectChanged();
}

void Qocoon::addResourceFolder()
{
//    String selectedFolder{ toString(QFileDialog::getExistingDirectory(
//                                        nullptr, tr("Add Resource Folder"),
//                                        toQString(projectLocation()))) };

//    if (selectedFolder.IsEmpty()) // Cancelled
//        return;

//    statusBar()->showMessage("Added resource folder " + toQString(selectedFolder));
//    project_->addResourceFolder(selectedFolder);
}

String Qocoon::trimmedResourceName(const String& fileName)
{
//    if (!project_->inResourceFolder(fileName))
        return "";
//    else
//        return fileName.Replaced(containingFolder(fileName), "");
}

String Qocoon::containingFolder(const String& path)
{
//    if (project_) for (String folder: project_->resourceFolders())
//    {
//        if (path.Contains(project()->absoluteResourceFolder(folder)))
//            return folder;
//    }

    return "";
}

String Qocoon::locateResourceRoot(String resourcePath)
{
//    if (resourcePath.IsEmpty())
//        return "";


//    if (project_ && project()->inResourceFolder(resourcePath))
//        return containingFolder(resourcePath);

//    String trail{ resourcePath };

//    while (GetParentPath(trail) != trail)
//    {
//        String projectFile{ trail + FILENAME_PROJECT };

//        if (QFileInfo(toQString(projectFile)).exists())
//            return trail;

//        trail = GetParentPath(trail);
//    }

    return "";
}

String Qocoon::projectLocation()
{
//    if (project_)
//        return GetPath(project_->location());
//    else
        return "";
}

void Qocoon::toggleFullView(bool toggled)
{
    QSettings settings{};

    if (toggled)
    {
        settings.setValue("geometry", saveGeometry());
        settings.setValue("state", saveState());

        for (QDockWidget* dock: findChildren<QDockWidget*>()) {
            if (dock->isVisible()) {
                dock->setVisible(false);
            }
        }

        statusBar()->setVisible(false);
        menuBar()->setVisible(false);
        mainToolBar_->setVisible(false);

        if (!isFullScreen())
            showFullScreen();

        fullViewAction_->setShortcuts({ QKeySequence("F11"), QKeySequence("Esc") });

    }
    else
    {
        statusBar()->setVisible(true);
        menuBar()->setVisible(true);

        if (isFullScreen())
            showMaximized();

        restoreGeometry(settings.value("geometry").toByteArray());
        restoreState(settings.value("state").toByteArray());

        fullViewAction_->setShortcuts({ QKeySequence("F11") });
    }
}

void Qocoon::resizeEvent(QResizeEvent* event)
{
    if (static_cast<double>(event->size().width()) / event->size().height() > 1.0) {

        setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);


    } else {

        setCorner(Qt::BottomRightCorner, Qt::BottomDockWidgetArea);
    }
}

void Qocoon::about()
{
    QString aboutText{ tr("<p>Copyleft 🄯 2020 <a href=\"https://luckeyproductions.nl\">LucKey Productions</a></b>"
                          "<p>You may use and redistribute this software under the terms "
                          "of the<br><a href=\"https://www.gnu.org/licenses/gpl.html\">"
                          "GNU General Public License Version 3</a>.</p>") };

    QDialog* aboutBox{ new QDialog(this) };

    aboutBox->setWindowTitle("About " + Weaver::applicationDisplayName());
    QVBoxLayout* aboutLayout{ new QVBoxLayout() };
    aboutLayout->setContentsMargins(0, 8, 0, 4);

    QPushButton* DollyButton{ new QPushButton() };
    QPixmap DollyLogo{ ":/Icon" };
    DollyButton->setIcon(DollyLogo);
    DollyButton->setFlat(true);
    DollyButton->setMinimumSize(DollyLogo.width() * 04, DollyLogo.height());
    DollyButton->setIconSize(DollyLogo.size());
    DollyButton->setToolTip("https://gitlab.com/luckeyproductions/dolly");
    DollyButton->setCursor(Qt::CursorShape::PointingHandCursor);
    aboutLayout->addWidget(DollyButton);
    aboutLayout->setAlignment(DollyButton, Qt::AlignHCenter);
    connect(DollyButton, SIGNAL(clicked(bool)), this, SLOT(openUrl()));

    QLabel* aboutLabel{ new QLabel(aboutText) };
    aboutLabel->setWordWrap(true);
    aboutLabel->setAlignment(Qt::AlignJustify);
    QVBoxLayout* labelLayout{ new QVBoxLayout() };
    labelLayout->setContentsMargins(42, 34, 42, 12);
    labelLayout->addWidget(aboutLabel);
    aboutLayout->addLayout(labelLayout);

    QDialogButtonBox* buttonBox{ new QDialogButtonBox(QDialogButtonBox::Ok, aboutBox) };
    connect(buttonBox, SIGNAL(accepted()), aboutBox, SLOT(accept()));
    aboutLayout->addWidget(buttonBox);

    aboutBox->setLayout(aboutLayout);
    aboutBox->resize(aboutBox->minimumSize());
    aboutBox->exec();
}
void Qocoon::openUrl()
{
    QDesktopServices::openUrl(QUrl(qobject_cast<QPushButton*>(sender())->toolTip()));
}

void Qocoon::undo()
{/*
    if (view3D_->scene()) {

        sceneStack(view3D_->scene())->undo();
    }
*/}
void Qocoon::redo()
{/*
    if (view3D_->scene()) {

        sceneStack(view3D_->scene())->redo();
    }
*/}
