TEMPLATE = app
TARGET = dolly

CONFIG += console c++11
CONFIG -= app_bundle
QT += core gui widgets
QMAKE_CXXFLAGS += -std=c++11 -O2

LIBS += \
    $${PWD}/Dry/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

INCLUDEPATH += \
    Dry/include \
    Dry/include/Dry/ThirdParty \

HEADERS += \
    bonewidget.h \
    garnish.h \
    skeletonwidget.h \
    weaver.h \
    qocoon.h \
    view3d.h \
    dry.h \
    jib.h \
    drywidget.h \
    drydockwidget.h

SOURCES += \
    bonewidget.cpp \
    garnish.cpp \
    main.cpp \
    skeletonwidget.cpp \
    weaver.cpp \
    qocoon.cpp \
    view3d.cpp \
    jib.cpp \
    drywidget.cpp \
    drydockwidget.cpp

DISTFILES += \
    .gitignore

RESOURCES += \
    dolly.qrc
