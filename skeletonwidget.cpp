/* Dolly
// Copyright (C) 2020 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QVBoxLayout>

#include "weaver.h"
#include "qocoon.h"

#include "skeletonwidget.h"

SkeletonWidget::SkeletonWidget(Qocoon* parent): DryWidget(parent->GetContext(), parent),
    nameLabel_{ new QLabel{ "Name" } },
    skeletonView_{ new QTreeWidget{} },
    model_{ nullptr }
{
    setObjectName("Skeleton");

    QVBoxLayout* mainLayout{ new QVBoxLayout{} };
    mainLayout->setMargin(0);

    nameLabel_->setEnabled(false);
    mainLayout->addWidget(nameLabel_);

    skeletonView_->setSelectionMode(QAbstractItemView::SingleSelection);
    skeletonView_->setHeaderHidden(true);
    skeletonView_->setMinimumHeight(040);
    mainLayout->addWidget(skeletonView_);

    setLayout(mainLayout);

    connect(parent, SIGNAL(activeModelChanged(AnimatedModel*)), this, SLOT(updateModel(AnimatedModel*)));
    connect(skeletonView_, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)),
            this, SLOT(handleItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)));
}

void SkeletonWidget::updateModel(AnimatedModel* model)
{
    String modelName{ "Name" };

    skeletonView_->clear();

    if (model->GetModel())
    {
        skeletonView_->clear();

        model_ = model;
        modelName = model_->GetNode()->GetName();

        for (const Bone& b: model_->GetSkeleton().GetBones())
        {
            const String nodeName{ b.node_->GetName() };
            const unsigned boneIndex{ model_->GetSkeleton().GetBoneIndex(&b) };

            if (boneIndex == b.parentIndex_)
            {
                QTreeWidgetItem* boneItem{ new QTreeWidgetItem{ { toQString(nodeName) } } };
                boneItem->setData(0, BoneIndex, boneIndex);

                skeletonView_->addTopLevelItem(boneItem);
                growBoneTree(boneItem);
            }
        }

        skeletonView_->expandAll();
    }
    else
    {
        model_ = nullptr;
    }

    setBone(-1);

    nameLabel_->setText(toQString(modelName));
    nameLabel_->setEnabled(model_);
}

void SkeletonWidget::growBoneTree(QTreeWidgetItem* parentItem)
{
    const unsigned parentIndex{ parentItem->data(0, BoneIndex).toUInt() };

    const Skeleton& skeleton{ model_->GetSkeleton() };

    for (const Bone& b: skeleton.GetBones())
    {
        const unsigned boneIndex{ skeleton.GetBoneIndex(&b) };

        if (boneIndex != parentIndex
         && b.parentIndex_ == parentIndex)
        {
            const String nodeName{ b.node_->GetName() };
            QTreeWidgetItem* boneItem{ new QTreeWidgetItem{ { toQString(nodeName) } } };
            boneItem->setData(0, BoneIndex, boneIndex);

            parentItem->addChild(boneItem);
            growBoneTree(boneItem);
        }
    }
}

void SkeletonWidget::handleItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous)
{
    if (current)
        setBone(current->data(0, BoneIndex).toUInt());
}

void SkeletonWidget::setBone(int index)
{
    if (!skeletonView_->selectedItems().isEmpty())
    {
        if (index == -1)
        {
            skeletonView_->setCurrentItem(nullptr);
            emit currentBoneChanged(nullptr);
            return;
        }

        QTreeWidgetItem* selectedItem{ skeletonView_->selectedItems().front() };
        const unsigned selectedIndex{ selectedItem->data(0, BoneIndex).toUInt() };

        if (index == selectedIndex)
            return;
    }

    if (index != -1)
    {
        QTreeWidgetItemIterator it{ skeletonView_ };

        while (*it)
        {
            if ((*it)->data(0, BoneIndex).toUInt() == index)
            {
                skeletonView_->setCurrentItem(*it);
                emit currentBoneChanged(model_->GetSkeleton().GetBone(index)->node_);
                return;
            }

            ++it;
        }
    }
}
